package main

import (
	"context"
	_ "database/docs"
	"database/internal/app/handler"
	"database/internal/app/repository"
	"database/internal/app/service"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

// @title           Selenium Parser API
// @version         1.0
// @description     API server for parsing habr vacancies
// @host      localhost:8080

func main() {

	/*	// загружаем переменные окружения из файла .env в корневой директории проекта
		err := godotenv.Load()
		if err != nil {
			log.Fatal().Msg("Error loading .env file")
		}
	*/
	// Getenv извлекает значение переменной среды, названной по ключу
	dbType := os.Getenv("DBDRIVER")

	var repo service.IRepository
	var err error
	switch dbType {
	case "postgres":
		repo, err = repository.NewPostgresDB()
		if err != nil {
			log.Fatal().Err(err)
		}
	case "mongodb":
		repo, err = repository.NewMongoDB()
		if err != nil {
			log.Fatal().Err(err)
		}
	case "RAM":
		repo, err = repository.NewMemoryDB()
		if err != nil {
			log.Fatal().Err(err)
		}
	default:
		log.Fatal().Msg("Unknown database type")
	}

	// создаем кэшированный репозиторий
	cachedRepo := repository.NewCachedRepository(repo)

	// передаем выбранный репозиторий в слои приложения
	hand := handler.NewHandler(cachedRepo)

	// запускаем сервер
	port := ":8080"
	server := http.Server{
		Addr:           port,
		Handler:        hand.InitRoutes(),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		log.Info().Msgf("Server starting at port %v", port)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Msg("Server stopped")
		}
	}()

	// ожидание сигнала
	quit := make(chan os.Signal, 1)
	// регистрирум обработчик для сигнала os.Interrupt
	signal.Notify(quit, os.Interrupt)
	// блокируем выполнение программы, пока не будет получен сигнал os. Interrupt
	<-quit
	log.Info().Msg("Shutdown Server")

	// тайм-аут завершения
	// создаем контекст ctx с таймаутом в 5 секунд и функцию cancel, которая может быть использована для отмены контекста
	// Если операция не завершится в течение указанного времени, контекст будет отменен и все горутины, связанные с этим контекстом, будут завершены.
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Msgf("Server shutdown failed, %v", err)
	}
	defer func(server *http.Server) {
		err := server.Close()
		if err != nil {

		}
	}(&server)

	log.Info().Msg("Server exiting")

}
