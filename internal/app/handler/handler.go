package handler

import (
	_ "database/docs"
	"database/internal/app/model"
	"database/internal/app/service"
	"database/internal/driver"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/rs/zerolog/log"
	"github.com/tebeka/selenium"
	"net/http"
	"time"
)

type Handler struct {
	service service.IService
}

func NewHandler(repo service.IRepository) *Handler {
	return &Handler{service.NewService(repo)}
}

// Search godoc
// @Summary      Search and parsing
// @Description  Search and parse vacancies from HABR career
// @Tags         selenium
// @Accept       json
// @Produce      json
// @Param   input   body   model.SwaggerQuery   true   "JSON object"
// @Failure      200
// @Failure      500
// @Router       /search [post]
func (h *Handler) Search(w http.ResponseWriter, r *http.Request) {
	inputJSON := make(map[string]string)
	err := json.NewDecoder(r.Body).Decode(&inputJSON)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	query, ok := inputJSON["query"]
	if !ok {
		http.Error(w, "No query in request", http.StatusBadRequest)
		return
	}
	rootURL := "https://career.habr.com"

	// запускаем драйвер
	wd, err := driver.RunFirefoxDriver()
	if err != nil {
		log.Fatal().Msg(err.Error())
	}
	defer func(wd selenium.WebDriver) {
		err := wd.Quit()
		if err != nil {
			log.Info().Msg("Driver close error")
		}
	}(wd)

	var links []string
	var page int

	for {
		page++
		// выполняем запрос на страницу с результатами поиска вакансий
		err = wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			continue
		}

		// ищем элемент с классом .search-total, чтобы убедиться, что мы находимся на странице с результатами поиска.
		// Если элемент не найден, то мы выходим из цикла
		_, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
		if err != nil {
			break
		}

		// ищем все элементы на странице, которые соответствуют CSS-селектору .vacancy-card__title-link.
		// Затем, для каждого элемента, мы получаем значение атрибута href, который содержит ссылку на вакансию
		elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			continue
		}
		for _, elem := range elems {
			link, err := elem.GetAttribute("href")
			if err != nil {
				continue
			}
			links = append(links, rootURL+link)
		}
	}

	log.Info().Msgf("Total vacancies - %v", len(links))

	var vacancy model.Vacancy

	// загружаем страницу по ссылке
	for _, link := range links {
		resp, err := http.Get(link)
		if err != nil {
			log.Error().Msg(err.Error())
			continue
		}
		// парсим body страницы
		var doc *goquery.Document
		doc, err = goquery.NewDocumentFromReader(resp.Body)
		if err != nil || doc == nil {
			log.Error().Msg(err.Error())
			continue
		}

		// ищем элементы, у которых атрибут type равен application/ld+json.
		dd := doc.Find("script[type=\"application/ld+json\"]")
		if dd == nil {
			log.Error().Msg(err.Error())
			continue
		}

		// декодируем первый элемент их найденных элементов, приведя значение к срезу байтов
		vacancy, err = model.UnmarshalVacancy([]byte(dd.First().Text()))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			continue
		}

		// передаем значение в метод сервиса
		err = h.service.NewVacancy(vacancy)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			continue
		}
		time.Sleep(1 * time.Second)
	}

	w.WriteHeader(http.StatusOK)

}

// GetAll
// @Summary      Get all vacancies
// @Description  Get a list of all vacancies
// @Tags         map
// @Accept       json
// @Produce      json
// @Success      200 {object} []model.Vacancy
// @Router       /list [get]
func (h *Handler) GetAll(w http.ResponseWriter, r *http.Request) {
	vacancies, err := h.service.GetVacancyList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(vacancies)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

// GetByID
// @Summary      Get by ID
// @Description  Get vacancies by ID
// @Tags         map
// @Accept       json
// @Produce      json
// @Param input body model.SwaggerID   true "JSON object"
// @Success      200 {object} model.Vacancy
// @Failure      404
// @Router       /get [get]
func (h *Handler) GetByID(w http.ResponseWriter, r *http.Request) {
	inputJSON := make(map[string]int)
	err := json.NewDecoder(r.Body).Decode(&inputJSON)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	id, ok := inputJSON["id"]
	if !ok {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vacancy, err := h.service.GetVacancyByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	err = json.NewEncoder(w).Encode(vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

// Delete
// @Summary      Delete vacancy
// @Description  Delete vacancy by ID
// @Tags         map
// @Accept       json
// @Produce      json
// @Param input body model.SwaggerID   true "JSON object"
// @Success      200
// @Failure      404
// @Router       /delete [delete]
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	inputJSON := make(map[string]int)
	err := json.NewDecoder(r.Body).Decode(&inputJSON)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	id, ok := inputJSON["id"]
	if !ok {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.service.DeleteVacancy(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
}
