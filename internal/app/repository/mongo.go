package repository

import (
	"context"
	"database/internal/app/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"sync"
)

type MongoDB struct {
	client     *mongo.Client
	collection *mongo.Collection
	sync.Mutex
}

// context.тоdo используется в данном случае как временный заглушечный контекст,
// который может быть использован в случаях, когда контекст не имеет никаких значений и не устанавливает параметры выполнения операции
// Однако, использование контекста является хорошей практикой при работе с MongoDB и другими базами данных, так как позволяет
// настроить параметры выполнения операции, такие как таймауты и отмена операции. Кроме того, использование контекста может
// улучшить производительность и надежность приложения. Вместо context.тоdo() можно использовать другие типы контекста, такие
// как context.Background(), который создает пустой контекст без каких-либо значений, или создать свой контекст с параметрами

func NewMongoDB() (*MongoDB, error) {
	// метод mongo.Connect создает новый клиент MongoDB и устанавливает соединение с сервером MongoDB,
	// используя параметры, которые были переданы в качестве аргументов - url и порт
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://mongodb:27017"))
	if err != nil {
		return nil, err
	}
	// метод client.Database("mongodb") возвращает объект базы данных MongoDB с именем "mongodb"
	// если базы данных с таким именем не существует, то она будет создана при выполнении первой операции на этой базе данных
	// метод Collection("vacancies") возвращает объект коллекции с именем vacancies в базе данных "mongodb"
	// если коллекции с таким именем не существует, то она будет создана при выполнении первой операции на этой коллекции
	collection := client.Database("mongodb").Collection("vacancies")

	return &MongoDB{
		client:     client,
		collection: collection,
	}, nil
}

func (m *MongoDB) AddVacancy(vacancy model.Vacancy) error {
	m.Lock()
	defer m.Unlock()
	// получаем количество документов в коллекции
	counter, err := m.collection.CountDocuments(context.TODO(), bson.M{})
	if err != nil {
		return err
	}
	// устанавливаем счетчик
	vacancy.ID = int(counter) + 1

	// вставляем запись vacancy в коллекцию базы данных
	_, err = m.collection.InsertOne(context.TODO(), vacancy)
	if err != nil {
		return err
	}
	return nil
}

func (m *MongoDB) GetByID(id int) (model.Vacancy, error) {
	var vacancy model.Vacancy
	// выполняет поиск документа в коллекции MongoDB, на которую ссылается переменная m.collection, с помощью метода FindOne
	// метод FindOne возвращает первый документ, удовлетворяющий критериям поиска, переданным в виде фильтра в виде bson.M{} - "id": id
	// если документ с заданным id найден, то метод Decode декодирует найденный документ в структуру vacancy
	err := m.collection.FindOne(context.TODO(), bson.M{"id": id}).Decode(&vacancy)
	if err != nil {
		return model.Vacancy{}, err
	}
	return vacancy, nil
}

func (m *MongoDB) GetList() ([]model.Vacancy, error) {
	var vacancies []model.Vacancy
	//  выполняет поиск документов в коллекции MongoDB, на которую ссылается переменная m.collection, с помощью метода Find.
	// Метод Find возвращает курсор (cursor), который позволяет итерироваться по результатам поиска.
	// В данном случае, мы передаем пустой фильтр в виде bson.D{{}}, что означает, что мы хотим получить все документы
	// в коллекции без каких-либо дополнительных условий фильтрации. Кроме того, мы передаем опции поиска в виде options.Find(),
	// которые позволяют настроить поведение метода Find. В данном случае, мы используем стандартные опции поиска
	cur, err := m.collection.Find(context.TODO(), bson.D{{}}, options.Find())
	if err != nil {
		return nil, err
	}
	// отложенное закрытие курсора, который был получен при выполнении запроса к коллекции MongoDB
	defer cur.Close(context.TODO())
	// перебираем все документы, которые были найдены в результате поиска. Метод Next возвращает true,
	// если есть следующий документ, и false, если документы закончились или произошла ошибка
	// внутри цикла мы декодируем каждый документ в структуру model.Vacancy с помощью метода Decode и добавляем в срез
	for cur.Next(context.TODO()) {
		var elem model.Vacancy
		err := cur.Decode(&elem)
		if err != nil {
			return nil, err
		}
		vacancies = append(vacancies, elem)
	}
	return vacancies, nil
}

func (m *MongoDB) Delete(id int) error {
	// метод DeleteOne удаляет первый документ, который соответствует заданному фильтру
	// в данном случае, мы передаем фильтр в виде bson.M{"id": id}
	_, err := m.collection.DeleteOne(context.TODO(), bson.M{"id": id})
	if err != nil {
		return err
	}
	return nil
}
