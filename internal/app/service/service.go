package service

import (
	"database/internal/app/model"
)

type IRepository interface {
	AddVacancy(vacancy model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type IService interface {
	NewVacancy(vacancy model.Vacancy) error
	GetVacancyByID(id int) (model.Vacancy, error)
	GetVacancyList() ([]model.Vacancy, error)
	DeleteVacancy(id int) error
}

type Service struct {
	repo IRepository
}

func NewService(repo IRepository) *Service {
	return &Service{repo: repo}
}

func (s *Service) NewVacancy(vacancy model.Vacancy) error {
	return s.repo.AddVacancy(vacancy)
}

func (s *Service) GetVacancyByID(id int) (model.Vacancy, error) {
	return s.repo.GetByID(id)
}

func (s *Service) GetVacancyList() ([]model.Vacancy, error) {
	return s.repo.GetList()
}

func (s *Service) DeleteVacancy(id int) error {
	return s.repo.Delete(id)
}
